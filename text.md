1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.

Цикли потрібні для виконання однієї і тієї самої дії (частини якогось коду) декілька разів підряд.   

2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

* Цикл WHILE використовується, коли потрібно повторювати блок коду, до тих пір поки умова не досягне falsе. 

do...while - коли потрібно виконання хоча б однієї умови, в незалежності від того чи був виконаний true чи ні.

* For - схожа на while, але її відмінність в тому, що ми знаємо скільки ітерацій буде виконано в циклі і є можливість задати додаткові налаштування.   


3. Що таке явне та неявне приведення (перетворення) типів даних у JS?

Явне перетворення - ми задаємо функцію або оператор перед значенням 
наприклад:
let firstNumber = Number("2"); - рядок перетворюється в число

Неявне - перетворення відбувається автоматично 
наприклад:
let result = ("2" + 9); - результат буде 29, оскільки автоматично додаються два рядки